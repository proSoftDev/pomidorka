<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Stock;

/* @var $this yii\web\View */
/* @var $model Stock */

?>
<div class="stock">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Акции</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <img src="<?= $model->getImage(); ?>" class="banner">
            </div>
            <div class="col-lg-12">
                <h2><?= $model->title; ?></h2>
            </div>
            <div class="col-lg-12 mb-5">
                <?= $model->content; ?>
            </div>
        </div>
    </div>
</div>