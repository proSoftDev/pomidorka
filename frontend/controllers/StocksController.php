<?php

namespace frontend\controllers;

use common\models\Menu;
use common\models\Stock;

class StocksController extends FrontendController
{
    public function actionIndex()
    {
        $menu  = Menu::findKey('stock_header');
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        return $this->render('index');
    }

    public function actionView($id)
    {
        $model  = Stock::findOne(['id' => $id]);
        $this->setMeta($model->metaName, $model->metaDesc, $model->metaKey);

        return $this->render('view', [
            'model'     => $model,
        ]);
    }
}