<?php

namespace frontend\controllers;

class BasketController extends FrontendController
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}