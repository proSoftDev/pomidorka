<?php
namespace frontend\controllers;

use common\models\Banner;
use common\models\BannerMobile;
use common\models\BannerMobileProduct;
use common\models\BannerProduct;
use common\models\Brand;
use common\models\Catalog;
use common\models\FavoritesProduct;
use common\models\Menu;
use common\models\Product;
use frontend\models\Basket;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $menu  = Menu::findKey('main');
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $banners    = Banner::activeBanner();

        return $this->render('index', [
            'banners'   => $banners,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
