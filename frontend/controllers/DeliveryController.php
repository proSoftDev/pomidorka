<?php

namespace frontend\controllers;

use common\models\Delivery;
use common\models\Menu;

class DeliveryController extends FrontendController
{
    public function actionIndex()
    {
        $menu  = Menu::findKey('delivery');
        $this->setMeta($menu->metaName, $menu->metaDesc, $menu->metaKey);

        $model  = Delivery::getOne();

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}