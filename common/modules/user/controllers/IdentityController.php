<?php

namespace common\modules\user\controllers;

use common\modules\user\models\LoginForm;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Class IdentityController
 * @package common\modules\user\controllers
 */
class IdentityController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['login', 'logout'],
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['login'],
                        'roles'   => ['?'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['logout'],
                        'roles'   => ['@'],
                    ],
                    [
                        'allow'        => false,
                        'actions'      => ['login'],
                        'roles'        => ['@'],
                        'denyCallback' => function ($rule, $action) {
                            return $this->redirect(['/site/index'], 200);
                        },
                    ],
                    [
                        'allow'        => false,
                        'actions'      => ['logout'],
                        'roles'        => ['?'],
                        'denyCallback' => function ($rule, $action) {
                            return $this->redirect(['/user/identity/login'], 200);
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Logs in a user.
     * @return mixed
     */
    public function actionLogin()
    {
        $model = new LoginForm();

        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        return $this->render('login', ['model' => $model]);
    }

    /**
     * Logs out the current user.
     * @return mixed
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();

        return $this->goHome();
    }

}
