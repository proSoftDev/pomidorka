<?php

namespace api\models;

use common\models\OrderedProducts;
use common\models\Orders;
use yii\base\Model;

class OrderForm extends Model
{
    public $username;
    public $status;
    public $address;
    public $email;
    public $phone;
    public $message;
    public $sum;
    public $apartment;
    public $storey;
    public $porch;
    public $intercom;
    public $amountMoney;
    public $statusPay;
    public $typePay;

    public $orderProducts;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status'        => 'Статус',
            'username'      => 'ФИО',
            'address'       => 'Адрес',
            'email'         => 'Электронная почта',
            'phone'         => 'Телефон',
            'message'       => 'Комментарий',
            'sum'           => 'Сумма заказа',
            'apartment'     => 'Квартира',
            'storey'        => 'Этаж',
            'porch'         => 'Подъезд',
            'intercom'      => 'Домофон',
            'amountMoney'   => 'Сумма наличных',
            'statusPay'     => 'Статус оплаты',
            'typePay'       => 'Тип оплаты',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            [['status'], 'integer'],

            [['address'], 'required'],
            [['address', 'apartment', 'email', 'phone'], 'string', 'max' => 128],

            [['storey', 'porch', 'intercom'], 'string', 'max' => 128],

            [['statusPay', 'typePay', 'amountMoney', 'sum'], 'integer'],

            [['message'], 'string'],


        ];
    }

    /**
     * Create new order
     * @return mixed
     */
    public function create()
    {
        $this->orderProducts    = \Yii::$app->request->post()['orderProducts'];
        if ($this->validate()) {

            $order                      = new Orders();
            $order->order_id            = time();
            $order->user_id             = \Yii::$app->user->identity->id ? \Yii::$app->user->identity->id : null;
            $order->username            = $this->username;
            $order->sum                 = $this->sum;
            $order->status              = 1;
            $order->address             = $this->address;
            $order->email               = $this->email;
            $order->phone               = $this->phone;
            $order->message             = $this->message;

            $order->apartment           = $this->apartment;
            $order->storey              = $this->storey;
            $order->porch               = $this->porch;
            $order->intercom            = $this->intercom;
            $order->amountMoney         = $this->amountMoney;
            $order->typePay             = $this->typePay;
            $order->statusPay           = 0;

            if($order->save()){
                foreach($this->orderProducts as $item){
                    $orderedProducts                = new OrderedProducts();
                    $orderedProducts->order_id      = $order->id;
                    $orderedProducts->product_id    = $item['id'];
                    $orderedProducts->detail_id     = $item['detail_id'];
                    $orderedProducts->count         = $item['quantity'];

                    $orderedProducts->save();
                }

            }

            return $order;
        }

        return false;
    }

    /**
     * Update new order
     * @param $user_id
     * @param $order_id
     * @return mixed
     */
    public function orderUpdate($user_id, $order_id)
    {
        $order                      = Orders::findOne(['id' => $order_id]);
        $order->user_id             = $user_id;
        $order->save();

        return $order;
    }
}