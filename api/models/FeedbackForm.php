<?php

namespace api\models;

use common\models\Feedback;
use common\models\User;
use yii\base\Model;

class FeedbackForm extends Model
{
    public $email;
    public $phone;
    public $theme;
    public $message;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email'         => 'Email',
            'phone'         => 'Телефон',
            'theme'         => 'Тема обращения',
            'message'       => 'Сообщение',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['theme', 'message'], 'required'],
            [['theme', 'message'], 'string'],
            [['email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * Create new feedback
     * @return mixed
     */
    public function create()
    {
        if ($this->validate()) {
            $feedback   = new Feedback();
            $feedback->user_id  = $this->getUser()->id;
            $feedback->phone    = $this->getUser()->phone;
            $feedback->email    = $this->getUser()->email;

            $feedback->theme    = $this->theme;
            $feedback->message  = $this->message;

            $feedback->save();

            return $feedback;
        }

        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user    = User::findOne(['id' => \Yii::$app->user->identity->id]);
        }

        return $this->_user;
    }
}