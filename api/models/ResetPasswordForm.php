<?php
namespace api\models;

use yii\base\InvalidArgumentException;
use yii\base\Model;
use common\models\User;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $login;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['login', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login'             => 'Почта или телефон',
        ];
    }

    /**
     * Resets password.
     *
     * @return mixed
     * @throws
     */
    public function resetPassword()
    {
        $user   = $this->getFindUser();
        if($user && $this->validate()){
            $password = \Yii::$app->security->generateRandomString(10);
            $user->setPassword($password);
            $user->save();
            $this->sendInformationAboutNewPassword($user->email, $password);

            return true;

        }

        return null;
    }

    /**
     * Resets password mobile.
     *
     * @param  $login
     * @return mixed
     * @throws
     */
    public function resetPasswordMobile($login)
    {
        $this->login    = $login;

        $user   = $this->getFindUser();
        if($user && $this->validate()){
            $password = \Yii::$app->security->generateRandomString(10);
            $user->setPassword($password);
            $user->save();
            $this->sendInformationAboutNewPassword($user->email, $password);

            return true;

        }

        return null;
    }

    /**
     * Finds user by [[login]]
     *
     * @return User|null
     */
    protected function getFindUser()
    {
        if ($this->_user === null) {
            if(strripos($this->login, '@')){
                $this->_user = User::findByEmail($this->login);
            }else {
                $this->_user = User::findByPhone($this->validatePhone($this->login));
            }
            return $this->_user;
        }
    }

    public function validatePhone($phone)
    {
        if(!($phone == null)){
            $response   = preg_replace("/[^0-9]/", "", $phone);

            return $response;
        }

        return null;
    }

    protected function sendInformationAboutNewPassword($email, $password)
    {
        $host = \Yii::$app->request->hostInfo;

        $emailSend = \Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => 'POMIDORKA'])
            ->setTo($email)
            ->setSubject('Восстановление доступа')
            ->setHtmlBody("<p>Вы получили данное письмо, т.к. на ваш e-mail был полуен запрос на восстановление пароля.</p> </br>
                                 </br>
                                 <p>Ваши данные для авторизации:</p> </br></br>
                                 <p>E-mail: $email</p>
                                 <p>Ваш новый пароль: $password</p> </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>«POMIDORKA»</a></p>");

        return $emailSend->send();

    }

    public function getRandomPassword()
    {
        return \Yii::$app->security->generateRandomString(10);
    }
}
