<?php

namespace api\controllers;

use api\models\LoginForm;
use yii\db\Exception;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;

class LoginController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionMobile($phone, $password)
    {
        $model  = new LoginForm();

        try {
            if ($client = $model->authMobile($phone, $password)) {
                $user   = $model->dataUser($client);
                $response   = [
                    'token' => $user['token'],
                    'user'  => $user
                ];

                return $response;
            } else {
                $errors = $model->firstErrors;
                throw new Exception(reset($errors));
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }
}