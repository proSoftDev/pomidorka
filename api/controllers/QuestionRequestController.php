<?php

namespace api\controllers;

use api\models\QuestionForm;
use yii\db\Exception;
use yii\rest\Controller;
use yii\web\HttpException;

class QuestionRequestController extends Controller
{
    public function actionCreate()
    {
        @header('Content-Type: application/json; charset=utf-8');

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400'); // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            exit(0);
        }

        $model  = new QuestionForm();

        if(\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post(), '')){

            try {
                if ($order = $model->create()) {
                    $response   = [
                        'status'    => 1,
                        'message'   => 'Вы успешно оставили свой вопрос!'];
                    return $response;
                } else {
                    $errors = $model->firstErrors;
                    \Yii::$app->response->setStatusCode(422);
                    throw new Exception(reset($errors));
                }
            }catch (\Exception $e){
                throw new HttpException(403, $e->getMessage());
            }

        }
    }
}