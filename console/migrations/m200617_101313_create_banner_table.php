<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%banner}}`.
 */
class m200617_101313_create_banner_table extends Migration
{
    public $table               = 'banner';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'status'            => $this->integer()->defaultValue(0)->null(),
            'title'             => $this->string(255)->null(),
            'image'             => $this->text()->null(),
            'delay'             => $this->integer()->null(),
            'link'              => $this->string(255)->null(),
            'sort'              => $this->integer()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
