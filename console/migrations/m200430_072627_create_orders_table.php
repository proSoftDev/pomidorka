<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%orders}}`.
 */
class m200430_072627_create_orders_table extends Migration
{
    public $table               = 'orders';
    public $userTable           = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'order_id'                  => $this->integer()->null(),
            'user_id'                   => $this->integer()->null(),
            'sum'                       => $this->integer()->null(),
            'status'                    => $this->integer()->defaultValue(1)->notNull(),
            'username'                  => $this->string(255)->null(),
            'phone'                     => $this->string(255)->null(),
            'email'                     => $this->string(128)->null(),
            'address'                   => $this->string(128)->null(),
            'apartment'                 => $this->string(128)->null(),
            'storey'                    => $this->string(128)->null(),
            'porch'                     => $this->string(128)->null(),
            'intercom'                  => $this->string(128)->null(),
            'message'                   => $this->text()->null(),
            'amountMoney'               => $this->string(128)->null(),
            'statusPay'                 => $this->integer()->defaultValue(0)->notNull(),
            'typePay'                   => $this->integer()->defaultValue(0)->notNull(),
            'created_at'                => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->userTable}", "{{{$this->table}}}", 'user_id', "{{{$this->userTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->userTable}_{$this->table}", "{{{$this->userTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
