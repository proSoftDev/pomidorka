<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%address}}`.
 */
class m200617_062322_create_address_table extends Migration
{
    public $table                   = 'address';
    public $cityTable               = 'cities';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'city_id'           => $this->integer()->defaultValue(0)->null(),
            'status'            => $this->integer()->defaultValue(0)->null(),
            'address'           => $this->string(255)->null(),
            'info'              => $this->text()->null(),
            'longitude'         => $this->text()->null(),
            'latitude'          => $this->text()->null(),
            'sort'              => $this->integer()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }

        $this->addForeignKey("fk_{$this->table}_{$this->cityTable}",
            "{{{$this->table}}}", 'city_id',
            "{{{$this->cityTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            "fk_{$this->table}_{$this->cityTable}",
            "{{{$this->cityTable}}}");

        $this->dropTable("{{{$this->table}}}");
    }
}
