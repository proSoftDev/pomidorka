<?php

use yii\db\Migration;

/**
 * Class m200622_043237_add_columns_contacts_table
 */
class m200622_043237_add_columns_contacts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contacts', 'play_market', $this->string(255)->null());
        $this->addColumn('contacts', 'app_store', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('contacts', 'play_market');
        $this->dropColumn('contacts', 'app_store');
    }
}
