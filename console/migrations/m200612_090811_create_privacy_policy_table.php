<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%privacy_policy}}`.
 */
class m200612_090811_create_privacy_policy_table extends Migration
{
    public $table               = 'privacy_policy';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'title'             => $this->string(255)->null(),
            'content'           => $this->text()->null(),
            'image'             => $this->text()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
