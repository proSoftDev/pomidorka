<?php

namespace backend\controllers;

use Yii;
use common\models\Catalog;
use backend\models\search\CatalogSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CatalogController implements the CRUD actions for Catalog model.
 */
class CatalogController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createCatalog',
        'view'   => 'viewCatalog',
        'update' => 'updateCatalog',
        'index'  => 'indexCatalog',
        'delete' => 'deleteCatalog',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Catalog::className();
            $this->searchModel = CatalogSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new Catalog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }

        $model = new Catalog();

        if ($model->load(Yii::$app->request->post())) {
            $this->createImage($model);
            $model->slug = $this->generateCyrillicToLatin(strip_tags($model->name));

            if($model->save()) {
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Catalog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }

        $model = $this->findModel($id);
        $oldImage = $model->image;

        if ($model->load(Yii::$app->request->post())) {
            $this->updateImage($model, $oldImage);
            $model->slug = $this->generateCyrillicToLatin(strip_tags($model->name));

            if($model->save()) {
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
